def is_perfect(number):
    divisors = []

    for curr_divisor in range(1, number):
        if number % curr_divisor == 0:
            divisors.append(curr_divisor)

    for curr_divisor in divisors:
        number -= curr_divisor

    return number == 0


if __name__ == "__main__":
    number = int(input("Enter a number: "))
    print(f"{number} is{'' if is_perfect(number) else ' not'} perfect")
